﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelRotation : MonoBehaviour {
    public Transform[] wheels;
    public float wheelRotationSpeed = 2f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < wheels.Length; i++)
        {
            wheels[i].Rotate(Vector3.forward * Time.deltaTime * wheelRotationSpeed);
        }
	}
}
