﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteeringWheelRotation: MonoBehaviour {
    [Range(-10f,10f)]
    public float rotaVehiculo = 0f;
    public Transform frontLeftWheel, frontRightWheel;
    public float wheelRotationDamp = 0.5f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, -rotaVehiculo, transform.eulerAngles.z);
        frontLeftWheel.eulerAngles = new Vector3(frontLeftWheel.eulerAngles.x,  -rotaVehiculo * wheelRotationDamp, frontLeftWheel.eulerAngles.z);
        frontRightWheel.eulerAngles = new Vector3(frontLeftWheel.eulerAngles.x, -rotaVehiculo * wheelRotationDamp, frontRightWheel.eulerAngles.z);
    }
}
